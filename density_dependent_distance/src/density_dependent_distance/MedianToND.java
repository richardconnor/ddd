package density_dependent_distance;

import eu.similarity.msc.data.MfAlexMetricSpace;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;

public class MedianToND {

    private static MfAlexMetricSpace mf;
    private static Map<Integer, float[]> data;

    public static void main(String[] args) {
        mf = new MfAlexMetricSpace("Volumes/data/mf_fc6/");
        data = mf.getData();
        Set<NdPair> ninds;
        Set<NdPair> inds;
        try {
            KnownNDs nds;
            nds = new KnownNDs("density_dependent_distance/nd_data/");
            ninds = nds.getNindPairs();
            inds = nds.getIndPairs();
        } catch (IOException ex) {
            ex.printStackTrace();
            return;
        }

        NearestDistances nind_distances = new NearestDistances();
        nind_distances.load("Volumes/Data/nonrelu_nind_distances.obj");
        NdPair[] nind_pairs = ninds.toArray(new NdPair[0]);

        NearestDistances ind_distances = new NearestDistances();
        nind_distances.load("Volumes/Data/nonrelu_ind_distances.obj");
        NdPair[] ind_pairs = inds.toArray(new NdPair[0]);

        File nind_file = new File("nind_differences.csv");
        File ind_file = new File("ind_differences.csv");

        try {
            nind_file.createNewFile();
            ind_file.createNewFile();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        try {
            System.out.println("Calcularing inds");
            calcDifferences(ind_pairs, ind_distances, "ind_differences.csv");
            System.out.println("Calculating ninds");
            calcDifferences(nind_pairs, nind_distances, "nind_differences.csv");
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

    private static void calcDifferences(NdPair[] pairs, NearestDistances distances, String out_file) throws IOException {
        FileWriter writer = new FileWriter(out_file);

        for (NdPair pair : pairs) {
            try {
                Double[] distances1 = distances.getImgNNs(pair.getId1());
                Arrays.sort(distances1);
                double median1 = distances1[50];
                Double[] distances2 = distances.getImgNNs(pair.getId2());
                Arrays.sort(distances2);
                double median2 = distances2[50];
                double distance = mf.getMetric().distance(data.get(pair.getId1()), data.get(pair.getId2()));
                double difference1 = median1 - distance;
                double difference2 = median2 - distance;
                double deviation1 = stdev(distances1);
                double deviation2 = stdev(distances2);
                double difference_in_stds1 = difference1 / deviation1;
                double difference_in_stds2 = difference2 / deviation2;
                writer.write(String.valueOf(difference_in_stds1) + "\n");
                writer.write(String.valueOf(difference_in_stds2) + "\n");
            } catch (Exception ignored) {
                System.out.println("Failed to find " + pair.getId1() + " and " + pair.getId2());
            }
        }
        writer.close();
    }

    private static double stdev(Double[] nums)
    {
        double sum = 0.0, standardDeviation = 0.0;
        int length = nums.length;

        for(double num : nums) {
            sum += num;
        }

        double mean = sum/length;

        for(double num: nums) {
            standardDeviation += Math.pow(num - mean, 2);
        }

        return Math.sqrt(standardDeviation/length);
    }
}
