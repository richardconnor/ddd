package density_dependent_distance;

import eu.similarity.msc.data.MfAlexMetricSpace;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class HundredNearest {

    public static void main(String[] args) {
        MfAlexMetricSpace mf = new MfAlexMetricSpace("Volumes/data/mf_fc6/");
        Map<Integer, float[]> data = mf.getData();
        Set<NdPair> nind_pairs;
        Set<NdPair> ind_pairs;
        try {
            KnownNDs nds;
            nds = new KnownNDs("density_dependent_distance/nd_data/");
            nind_pairs = nds.getNindPairs();
            ind_pairs = nds.getIndPairs();
        } catch (IOException ex) {
            ex.printStackTrace();
            return;
        }
        int total = nind_pairs.size();
        NearestDistances distances = new NearestDistances();
        distances.load("Volumes/Data/nonrelu_nind_distances.obj");
        System.out.print("Progress: " + String.valueOf(distances.distances.size()/2.0f/total) + "%" + String.format("(%d/%d)", distances.distances.size()/2, total));
        Object[] pairs = nind_pairs.toArray();
        for (int i = distances.distances.size()/2; i < total; i++) {
            int[] ids = {((NdPair)pairs[i]).getId1(), ((NdPair) pairs[i]).getId2()};
            for (int id : ids) {
                NavigableMap<Double, Integer> new_distances = new TreeMap<>();
                for (Iterator<Integer> it = data.keySet().iterator(); it.hasNext();) {
                    int key = it.next();
                    if (key == id) {
                        continue;
                    }
                    double dist = mf.getMetric().distance(data.get(id), data.get(key));
                    if (new_distances.size() < 100 || dist < new_distances.lastKey()) {
                        new_distances.put(dist, key);
                    }
                    if (new_distances.size() > 100) {
                        new_distances.remove(new_distances.lastKey());
                    }
                }
                Map<Integer, Double> swapped_distances = new_distances.entrySet().stream().collect(Collectors.toMap(Map.Entry::getValue, Map.Entry::getKey));
                distances.addImg(id, swapped_distances);
            }
            distances.save("Volumes/Data/nonrelu_nind_distances.obj");
            System.out.print("\r");
            System.out.print("                                                                                                               ");
            System.out.print("\r");
            System.out.print(String.format("Progress: %.2f", distances.distances.size()/0.02f/total) + "%" + String.format("(%d/%d)", distances.distances.size()/2, total));
        }
    }
}
