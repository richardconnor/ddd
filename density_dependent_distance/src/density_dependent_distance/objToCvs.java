package density_dependent_distance;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

public class objToCvs {

    public static void main(String[] args) {
        NearestDistances nind_distances = new NearestDistances();
        nind_distances.load("Volumes/Data/nonrelu_nind_distances.obj");
        NearestDistances ind_distances = new NearestDistances();
        ind_distances.load("Volumes/Data/nonrelu_ind_distances.obj");

        File nind_file = new File("nonrelu_nind_distances.csv");
        File ind_file = new File("nonrelu_ind_distances.csv");

        try {
            nind_file.createNewFile();
            ind_file.createNewFile();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        try {
            FileWriter writer = new FileWriter("nonrelu_nind_distances.csv");
            Map<Integer, Map<Integer, Double>> distances = nind_distances.distances;
            for (Map<Integer, Double> list : distances.values()) {
                Double[] vals = list.values().toArray(new Double[0]);
                Arrays.sort(vals);
                StringBuilder builder = new StringBuilder();
                builder.append(vals[0]);
                for (int i = 1; i < vals.length; i++) {
                    builder.append(",");
                    builder.append(vals[i]);
                }
                writer.write(builder.toString());
                writer.write("\n");
            }
            writer.close();

            writer = new FileWriter("nonrelu_ind_distances.csv");
            distances = ind_distances.distances;
            for (Map<Integer, Double> list : distances.values()) {
                Double[] vals = list.values().toArray(new Double[0]);
                Arrays.sort(vals);
                StringBuilder builder = new StringBuilder();
                builder.append(vals[0]);
                for (int i = 1; i < vals.length; i++) {
                    builder.append(",");
                    builder.append(vals[i]);
                }
                writer.write(builder.toString());
                writer.write("\n");
            }
            writer.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
