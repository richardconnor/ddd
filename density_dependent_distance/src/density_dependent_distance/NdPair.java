package density_dependent_distance;

public class NdPair implements Comparable<NdPair> {
	private int id1, id2;

	public NdPair(int id1, int id2) {
		if (id1 == id2) {
			throw new RuntimeException(this.getClass().getName() + ": can't create pair with identical keys");
		}
		this.id1 = Math.min(id1, id2);
		this.id2 = Math.max(id1, id2);
	}

	@Override
	public int compareTo(NdPair o) {
		NdPair that = (NdPair) o;
		if (that.getId1() < this.getId1()) {
			return 1;
		} else if (that.getId1() == this.getId1()) {
			if (that.getId2() == this.getId2()) {
				return 0;
			} else {
				if (that.getId2() < this.getId2()) {
					return 1;
				} else {
					return -1;
				}
			}
		} else {
			return -1;
		}
	}

	@Override
	public String toString() {
		return "<" + getId1() + "," + getId2() + ">";
	}

	public int getId1() {
		return this.id1;
	}

	public int getId2() {
		return this.id2;
	}

}
