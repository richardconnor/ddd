package density_dependent_distance;

import eu.similarity.msc.data.DecafMetricSpace;

import java.io.*;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class NearestDistances {
    public Map<Integer, Map<Integer, Double>> distances;

    public NearestDistances() {
        distances = new HashMap<>();
    }

    public boolean save(String location) {
        try {
            ObjectOutputStream obj_writer = new ObjectOutputStream(new FileOutputStream(location));
            obj_writer.writeObject(distances);
            obj_writer.flush();
            obj_writer.close();
        } catch (IOException ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean load(String location) {
        try {
            ObjectInputStream obj_reader = new ObjectInputStream(new FileInputStream(location));
            distances = (Map<Integer, Map<Integer, Double>>) obj_reader.readObject();
        } catch (IOException | ClassNotFoundException | ClassCastException ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    public Double[] getImgNNs(int id) {
        return distances.get(id).values().toArray(new Double[0]);
    }

    public void addImg(int id, Map<Integer, Double> distances) {
        this.distances.put(id, distances);
    }

}
