package density_dependent_distance;

import java.util.Map;

import eu.similarity.msc.data.MfAlexMetricSpace;

public class Test {

	public static void main(String[] args) {

		ReluAlexMetricSpace mf = new ReluAlexMetricSpace("Volumes/data/mf_fc6/");
		Map<Integer, float[]> data = mf.getData();
		float[] p1 = data.get(0);
		float[] p2 = data.get(585585);
		float[] p3 = data.get(69);
		float[] p4 = data.get(587979);

		System.out.println(mf.getMetric().distance(p1, p2));
		System.out.println(mf.getMetric().distance(p3, p4));
		System.out.println(mf.getMetric().distance(p1, p3));
		System.out.println(mf.getMetric().distance(p2, p4));
	}

}
