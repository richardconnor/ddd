package density_dependent_distance;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

import eu.similarity.msc.core_concepts.Metric;
import eu.similarity.msc.data.GistMetricSpace;
import eu.similarity.msc.data.MetricSpaceResource;
import eu.similarity.msc.data.MfAlexMetricSpace;

public class KnownNDs {

	private String pathRoot;
	private Set<NdPair> dups;
	private Set<NdPair> politoNinds;
	private Set<NdPair> strathNinds;
	private Set<NdPair> strathInds;
	private Set<NdPair> politoInds;

	public KnownNDs(String pathroot) throws IOException {
		this.pathRoot = pathroot;
		this.dups = getClustersAsNdPairs(pathRoot + "duplicates.txt");
		this.politoInds = getFileClustersAsPairs(false, false);
		this.strathInds = getFileClustersAsPairs(false, true);
		this.strathNinds = getFileClustersAsPairs(true, true);
		this.politoNinds = getFileClustersAsPairs(true, false);
	}

	public static void main(String[] args) throws IOException, ClassNotFoundException {

		KnownNDs knds = new KnownNDs("/Volumes/Data/mfnd/truthfiles/");
		GistMetricSpace gist = new GistMetricSpace("/Volumes/Data/mf_gist/");
		MfAlexMetricSpace mfa = new MfAlexMetricSpace("/Volumes/Data/mf_fc6/");
		MetricSpaceResource<Integer, float[]> space = mfa;

//		System.out.println("poli inds/dups");
//		Set<NdPair> poliDups = checkNoOverlap(politoInds, dups);
//		System.out.println("poli ninds/dups");
//		poliDups.addAll(checkNoOverlap(politoNinds, dups));
//
//		for (Set<Integer> cluster : createClusters(poliDups)) {
//			for (int i : cluster) {
//				System.out.print(i + " ");
//			}
//			System.out.println();
//		}

		System.out.println("strath inds/dups");
		getSetUnion(knds.strathInds, knds.dups, true);
		System.out.println("strath ninds/dups");
		getSetUnion(knds.strathNinds, knds.dups, true);
		System.out.println("polito inds/dups");
		Set<NdPair> politoIndDupOverlap = getSetUnion(knds.politoInds, knds.dups, false);

		for (Set<Integer> cluster : createClusters(politoIndDupOverlap)) {
			for (int i : cluster) {
				System.out.print(i + " ");
			}
			System.out.println();
		}

//		System.out.println("polito ninds/dups");
//		Set<NdPair> politoNindDupOverlap = checkNoOverlap(politoNinds, dups);

	}

	public Set<NdPair> getIndPairs() throws IOException {
		final Set<NdPair> strathInds = getFirstPairs(false, true);
		final Set<NdPair> politoInds = getFirstPairs(false, false);
		return getSetUnion(strathInds, politoInds, false);
	}

	public Set<NdPair> getNindPairs() throws IOException {
		return getSetUnion(getFirstPairs(true, true), getFirstPairs(true, false), false);
	}

	private static Set<NdPair> getSetUnion(Set<NdPair> set1, Set<NdPair> set2, boolean report) {
		Set<NdPair> res = new TreeSet<>();
		res.addAll(set1);
		res.addAll(set2);

		if (report) {
			boolean hasOverlap = res.size() < set1.size() + set2.size();
			System.out.println(hasOverlap ? "overlap found" : "no overlap");
		}

		return res;
	}

	private static void printDistances(MetricSpaceResource<Integer, float[]> space, Set<NdPair> ds1)
			throws IOException, ClassNotFoundException {
		Map<Integer, float[]> data = space.getData();
		Metric<float[]> jsd = space.getMetric();
		for (NdPair pair : ds1) {
			System.out.println(pair.getId1() + "\t" + pair.getId2() + "\t"
					+ jsd.distance(data.get(pair.getId1()), data.get(pair.getId2())));
		}
	}

	private Set<NdPair> getFileClustersAsPairs(boolean ninds, boolean strath) throws IOException {
		final String strathInds = strath ? pathRoot + "strathclyde/" + (ninds ? "N" : "") + "IND_clusters.txt"
				: pathRoot + "polito/" + (ninds ? "N" : "") + "IND_clusters.txt";
		return getClustersAsNdPairs(strathInds);
	}

	private Set<NdPair> getFirstPairs(boolean ninds, boolean strath) throws IOException {
		final String pathname = strath ? pathRoot + "strathclyde/" + (ninds ? "N" : "") + "IND_clusters.txt"
				: pathRoot + "polito/" + (ninds ? "N" : "") + "IND_clusters.txt";
		return getFirstPairsFromFile(pathname);
	}

	/**
	 * @param pathname file containing image id clusters, one cluster per line
	 *                 separated by spaces
	 * @return a set of pairs; each cluster of size n will result in n \choose 2
	 *         pairs
	 * @throws IOException
	 */
	@SuppressWarnings("boxing")
	private static Set<NdPair> getClustersAsNdPairs(String pathname) throws IOException {
		Set<NdPair> res = new TreeSet<>();
		FileReader fr = new FileReader(pathname);
		LineNumberReader lnr = new LineNumberReader(fr);
		for (String line = lnr.readLine(); line != null; line = lnr.readLine()) {
			Scanner s = new Scanner(line);
			List<Integer> cluster = new ArrayList<>();
			while (s.hasNextInt()) {
				cluster.add(s.nextInt());
			}
			s.close();
			for (int i = 0; i < cluster.size() - 1; i++) {
				for (int j = i + 1; j < cluster.size(); j++) {
					res.add(new NdPair(cluster.get(i), cluster.get(j)) );
				}
			}
		}
		return res;
	}

	private static Set<NdPair> getFirstPairsFromFile(String pathname) throws IOException {
		Set<NdPair> res = new TreeSet<>();
		FileReader fr = new FileReader(pathname);
		BufferedReader br = new BufferedReader(fr);
		for (String line = br.readLine(); line != null; line = br.readLine()) {
			Scanner s = new Scanner(line);
			int id1 = s.nextInt();
			int id2 = s.nextInt();

			res.add(new NdPair(id1, id2));

			s.close();
		}
		br.close();
		return res;
	}

	private static List<Set<Integer>> createClusters(Collection<NdPair> pairs) {
		List<Set<Integer>> res = new ArrayList<>();
		for (NdPair ndp : pairs) {
			Set<Integer> n = new TreeSet<>();
			n.add(ndp.getId1());
			n.add(ndp.getId2());
			res.add(n);
		}
		// now, cross-compare all sets and amalgamate any that overlap
		boolean foundOverlap = true;
		while (foundOverlap) {
			foundOverlap = false;
			List<Integer> redundant = new ArrayList<>();
			for (int i = 0; i < res.size() - 1; i++) {
				for (int j = i + 1; j < res.size(); j++) {
					Set<Integer> as = res.get(i);
					Set<Integer> bs = res.get(j);
					for (int a : as) {
						if (bs.contains(a)) {
							bs.addAll(as);
							if (!redundant.contains(i)) {
								redundant.add(i);
							}
						}
					}
				}
			}
			for (int i = redundant.size() - 1; i >= 0; i--) {
				res.remove(res.get(redundant.get(i)));
				foundOverlap = true;
			}
		}

		return res;
	}
}
