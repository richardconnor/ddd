package density_dependent_distance;

import eu.similarity.msc.core_concepts.Metric;
import eu.similarity.msc.data.IncrementalBuildMetricSpace;
import eu.similarity.msc.data.MfAlexMetricSpace;
import eu.similarity.msc.metrics.floats.Euclidean;

import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.logging.Logger;


/*
    A class to load in the data while applying the relu function to it.
 */
public class ReluAlexMetricSpace extends MfAlexMetricSpace {

    String rawDataFilePath;

    public ReluAlexMetricSpace(String filePath) {
        super(filePath);
    }

    public ReluAlexMetricSpace(String filePath, String rawDataFilePath) {
        super(filePath, rawDataFilePath);
        this.rawDataFilePath = rawDataFilePath;
    }

    @Override
    protected Map<Integer, float[]> getRawDataHunk(int fileNumber) {
        Map<Integer, float[]> hunk = new TreeMap<>();
        try {
            final FileReader fr = new FileReader(this.rawDataFilePath + fileNumber + ".txt");
            LineNumberReader lnr = new LineNumberReader(fr);
            for (String line = lnr.readLine(); line != null; line = lnr.readLine()) {
                Scanner s = new Scanner(line);
                int id = s.nextInt();
                hunk.put(id, relu(s));
                s.close();
            }
            lnr.close();
        } catch (IOException o) {
            Logger.getLogger(this.getClass().getName())
                    .severe("can't read from file " + this.rawDataFilePath + fileNumber + ".txt");
            throw new RuntimeException(this.getClass().getName());
        }

        return hunk;
    }

    private float[] relu(Scanner s) {
        float[] data = new float[4096];
        float sum = 0;
        for (int fl = 0; fl < 4096; fl++) {
            data[fl] = s.nextFloat();
            if (data[fl] < 0) data[fl] = 0;
            sum += data[fl] * data[fl];
        }
        sum = (float)Math.sqrt(sum);
        for (int fl = 0; fl < 4096; fl++) {
            data[fl] /= sum;
        }
        return data;
    }
}
